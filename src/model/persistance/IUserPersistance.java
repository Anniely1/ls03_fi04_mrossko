package model.persistance;

import model.User;

public interface IUserPersistance {

    User readUser(String username);

    int createUser(User user);

    void deleteUser(int userID);

    void updateUser(User user);

}