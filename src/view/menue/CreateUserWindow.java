package view.menue;

import controller.UserController;
import model.User;

import javax.swing.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class CreateUserWindow {
    private JTextField name;
    private JTextField vorname;
    private JTextField username;
    private JTextField geburtsdatum;
    private JTextField strasse;
    private JTextField hausnummer;
    private JTextField ort;
    private JTextField plz;
    private JPasswordField passwordField1;
    private JTextField gelaltserwartung;
    private JTextField note;
    private JPanel createUser;
    private JButton erstellenButton;
    private JTextField familienstand;
    private JTextArea response;


    public CreateUserWindow(UserController userController) {
        JFrame frame = new JFrame("User erstellen");
        frame.setContentPane(this.createUser);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        erstellenButton.addActionListener(e -> {
            if (validateUserData()) {
                try {
                    userController.createUser(new User(0, vorname.getText(),
                            name.getText(), LocalDate.parse(geburtsdatum.getText()),
                            strasse.getText(), hausnummer.getText(), plz.getText(), ort.getText(), username.getText(), Arrays.toString(passwordField1.getPassword()), Integer.parseInt(gelaltserwartung.getText()), familienstand.getText(), Double.parseDouble(note.getText())));

                } catch (NumberFormatException numberFormatException) {
                    response.setText("Keine Gültige eingabe");
                }
                frame.dispose();
            }
            response.setText("Bitte alle Felder ausfüllen!");

        });

    }

    private boolean validateUserData() {
        List<String> userData = List.of(name.getText(), vorname.getText(), username.getText(), geburtsdatum.getText(), strasse.getText(), hausnummer.getText(), ort.getText(), plz.getText(), gelaltserwartung.getText(), note.getText(), familienstand.getText());
        for (String userEntry : userData) {
            if (userEntry == null || userEntry.isEmpty())
                return false;
        }
        return true;
    }
}

