package controller;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

/**
 * controller for users
 *
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

    private IUserPersistance userPersistance;

    public UserController(IPersistance persistence) {
        this.userPersistance = persistence.getUserPersistance();
    }

    /**
     * erstelt eine neuen User, stellt sicher dass dieser noch nicht existiert
     * @param user das User Objekt welches erstellt werden soll
     * @return den int welcehr die User-ID repräsentiert, -1 falls der User bereits exisiert
     */
    public int createUser(User user) {
        User readUser = readUser(user.getLoginname(), null);
        if (readUser != null) {
            return userPersistance.createUser(user);
        }
        return -1;
    }

    /**
     * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
     *
     * @param username eindeutige Loginname
     * @param passwort das richtige Passwort
     * @return Userobjekt, null wenn der User nicht existiert
     */
    public User readUser(String username, String passwort) {
        return userPersistance.readUser(username);
    }

    /**
     * ändert die gespeicherten Daten eines users
     * @param user das Userobjekt welches die neuen Daten enthält
     */
    public void changeUser(User user) {
        userPersistance.updateUser(user);
    }

    /**
     * löscht einen User
     * @param user der User als Object welcher gelöscht werden soll
     */
    public void deleteUser(User user) {
        userPersistance.deleteUser(user.getP_user_id());

    }

    /**
     * prüft das eingabe Passwort gegen das in der Datenbank gespeicherte
     * @param username der Login name des Users
     * @param passwort das eingegebene Passwort des Users
     * @return
     */
    public boolean checkPassword(String username, String passwort) {
        User user = this.readUser(username, passwort);
        return user.getPassword().equals(passwort);
    }

    public IUserPersistance getUserPersistance() {
        return userPersistance;
    }
}
