import javax.swing.*;
import java.awt.*;

public class FormAendern {
    private JButton bntRedBackground;
    private JButton bntGreenBackground;
    private JButton bntBlueBackground;
    private JButton bntYellowBackground;
    private JButton bntBlankBackground;
    private JButton bntChooseColour;
    private JButton bntFontAriel;
    private JButton bntFontComicSans;
    private JButton bntFontCourierNew;
    private JTextField tfdTextEingabe;
    private JButton bntChangeText;
    private JButton bntDeleteText;
    private JButton bntRedText;
    private JButton bntBlueText;
    private JButton bntBlackText;
    private JButton bntIncreaseFontSize;
    private JButton bntDecreaseFontSize;
    private JButton bntLeftAlignText;
    private JButton bntCenterText;
    private JButton bntRightAlignText;
    private JButton bntExit;
    private JPanel pnlMain;
    private JPanel pnlSub1;
    private JPanel pnlSub2;
    private JTextField tfdText;

    public FormAendern() {
        bntRedBackground.addActionListener(e -> {
            pnlMain.setBackground(Color.RED);
            pnlSub2.setBackground(Color.RED);
            pnlSub1.setBackground(Color.RED);

        });
        bntGreenBackground.addActionListener(e -> {

            pnlMain.setBackground(Color.GREEN);
            pnlSub1.setBackground(Color.GREEN);
            pnlSub2.setBackground(Color.GREEN);

        });
        bntBlueBackground.addActionListener(e -> {
            pnlMain.setBackground(Color.BLUE);
            pnlSub2.setBackground(Color.BLUE);
            pnlSub1.setBackground(Color.BLUE);

        });
        bntYellowBackground.addActionListener(e -> {
            pnlMain.setBackground(Color.yellow);
            pnlSub2.setBackground(Color.yellow);
            pnlSub1.setBackground(Color.yellow);

        });
        bntBlankBackground.addActionListener(e -> {
            pnlMain.setBackground(null);
            pnlSub2.setBackground(null);
            pnlSub1.setBackground(null);

        });
        bntFontAriel.addActionListener(e -> tfdText.setFont(new Font("Ariel", Font.PLAIN, tfdText.getFont().getSize())));
        bntFontComicSans.addActionListener(e -> tfdText.setFont(new Font("Comic Sans MS", Font.PLAIN, tfdText.getFont().getSize())));

        bntFontCourierNew.addActionListener(e -> tfdText.setFont(new Font("Courier New", Font.PLAIN, tfdText.getFont().getSize())));
        bntChangeText.addActionListener(e -> tfdText.setText(tfdTextEingabe.getText()));
        bntDeleteText.addActionListener(e -> tfdText.setText(null));
        bntRedText.addActionListener(e -> tfdText.setForeground(Color.RED));
        bntBlueText.addActionListener(e -> tfdText.setForeground(Color.BLUE));
        bntBlackText.addActionListener(e -> tfdText.setForeground(Color.BLACK));
        bntIncreaseFontSize.addActionListener(e -> {
            int size = tfdText.getFont().getSize();
            tfdText.setFont(new Font(tfdText.getFont().getName(), Font.PLAIN, ++size));
        });
        bntDecreaseFontSize.addActionListener(e -> {
            int size = tfdText.getFont().getSize();
            tfdText.setFont(new Font(tfdText.getFont().getName(), Font.PLAIN, --size));
        });
        bntLeftAlignText.addActionListener(e -> tfdText.setHorizontalAlignment(SwingConstants.LEFT));
        bntCenterText.addActionListener(e -> tfdText.setHorizontalAlignment(SwingConstants.CENTER));
        bntRightAlignText.addActionListener(e -> tfdText.setHorizontalAlignment(SwingConstants.RIGHT));
        bntExit.addActionListener(e -> System.exit(0));
        bntChooseColour.addActionListener(e -> {

            Color pickedColour = JColorChooser.showDialog(null,
                    "Farbauswahl", null);
            pnlMain.setBackground(pickedColour);
            pnlSub2.setBackground(pickedColour);
            pnlSub1.setBackground(pickedColour);
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("my Gui");
        frame.setContentPane(new FormAendern().pnlMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}